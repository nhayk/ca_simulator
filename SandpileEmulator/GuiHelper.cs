﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Xml;
using System.Xml.Serialization;

namespace SandpileEmulator
{
    public static class GuiHelper
    {
        public static event EventHandler GrainAdded;
        private static Viewport3D _mainViewPort;
        private static int _size;
        private static List<InteractiveSphere> _points;
        public static List<InteractiveSphere> Points
        {
            get { return _points; }
        }

        public static void Init(Viewport3D vp)
        {
            _mainViewPort = vp;
        }

        public static void CreateNewModel(int size)
        {
            _size = size;
            _points = new List<InteractiveSphere>();
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    for (int z = 0; z < size; z++)
                    {
                        _points.Add(new InteractiveSphere(new Point3D(x, y, z)));
                    }
                }
            }
        }

        public static void DrawSandpileModel()
        {
            if (_mainViewPort.Children.Any(c => c is InteractiveSphere))
            {
                for (int i = _mainViewPort.Children.Count - 1; i >= 0; i--)
                {
                    if (_mainViewPort.Children[i] is InteractiveSphere)
                    {
                        _mainViewPort.Children.RemoveAt(i);
                    }
                }
            }

            foreach (var p in _points)
            {
                p.Draw();
                p.Transform = new TranslateTransform3D { OffsetX = (p.Position.X - (int)_size / 2) * 3, OffsetY = (p.Position.Y - (int)_size / 2) * 3, OffsetZ = (p.Position.Z - (int)_size / 2) * 3 };
                p.Visual = new Canvas {Width = 1, Height = 1, Background = GetColorByWeight(p.Weight)};
                p.Draw();
                _mainViewPort.Children.Add(p);
            }            
        }

        public static void AddGrain(int x, int y, int z)
        {
            var point = Points.FirstOrDefault(p => p.Position.X == x && p.Position.Y == y && p.Position.Z == z);
            if (point != null)
            {
                addGrainOnVertex(point);
            }
            if(GrainAdded != null)
            {
                GrainAdded(null, null);
            }
        }

        private static void addGrainOnVertex(InteractiveSphere point)
        {
            if(point != null)
            {
                point.Weight++;
                if (point.Weight > 7)
                {
                    point.Weight -= 7;
                    double x = point.Position.X;
                    double y = point.Position.Y;
                    double z = point.Position.Z;

                    addGrainOnVertex(GuiHelper.Points.FirstOrDefault(p => p.Position.X == x + 1 && p.Position.Y == y && p.Position.Z == z));
                    addGrainOnVertex(GuiHelper.Points.FirstOrDefault(p => p.Position.X == x && p.Position.Y == y + 1 && p.Position.Z == z));
                    addGrainOnVertex(GuiHelper.Points.FirstOrDefault(p => p.Position.X == x && p.Position.Y == y && p.Position.Z == z + 1));
                    addGrainOnVertex(GuiHelper.Points.FirstOrDefault(p => p.Position.X == x - 1 && p.Position.Y == y && p.Position.Z == z));
                    addGrainOnVertex(GuiHelper.Points.FirstOrDefault(p => p.Position.X == x && p.Position.Y == y - 1 && p.Position.Z == z));
                    addGrainOnVertex(GuiHelper.Points.FirstOrDefault(p => p.Position.X == x && p.Position.Y == y && p.Position.Z == z - 1));

                }
            }

        }

        private static Brush GetColorByWeight(int weight)
        {
            switch (weight)
            {
                case 0:
                    return Brushes.LightGray;
                case 1:
                    return Brushes.Yellow;
                case 2:
                    return Brushes.Red;
                case 3:
                    return Brushes.Green;
                case 4:
                    return Brushes.Blue;
                case 5:
                    return Brushes.Brown;
                case 6:
                    return Brushes.DarkOrange;
                default:
                    return Brushes.Black;
            }
        }
        

        #region File/String IO

        public static void LoadFromFile(string fileName)
        {
            XmlReader reader = XmlReader.Create(fileName);
            _points = new List<InteractiveSphere>();
            reader.MoveToContent();
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    InteractiveSphere obj = new InteractiveSphere();
                    obj.ReadXml(reader);
                    _points.Add(obj);
                }
            }
            reader.Close();
            DrawSandpileModel();
        }

        public static void WriteToFile(string fileName)
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<InteractiveSphere>));
            TextWriter writer = new StreamWriter(fileName);
            ser.Serialize(writer, Points);
            writer.Close();
        }

        public static string WriteToText()
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<InteractiveSphere>));
            using (MemoryStream stream = new MemoryStream())
            {
                TextWriter writer = new StreamWriter(stream);
                ser.Serialize(writer, Points);                
                stream.Position = 0;
                StreamReader r = new StreamReader(stream);
                var txt = r.ReadToEnd();
                writer.Close();
                r.Close();
                return txt;
            }          
        }

        public static void LoadFromText(string xml)
        {
            TextReader txtReader = new StringReader(xml);
            XmlReader reader = XmlReader.Create(txtReader);
            _points = new List<InteractiveSphere>();
            reader.MoveToContent();
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    InteractiveSphere obj = new InteractiveSphere();
                    obj.ReadXml(reader);
                    _points.Add(obj);
                }
            }
            reader.Close();
            DrawSandpileModel();
        }

        #endregion
    }
}
