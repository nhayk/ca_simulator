﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Media3D;

namespace SandpileEmulator
{
    public class ZoomToZConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 100;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int zoom = 10;
            if (!int.TryParse(System.Convert.ToString(value), out zoom))
            {
                zoom = 10;
            }
            return new Point3D { X = 0, Y = 0, Z = 10000 / zoom };
        }
    }
}
