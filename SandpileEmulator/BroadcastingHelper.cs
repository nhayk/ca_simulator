﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Controls;
using System.Windows.Threading;
using SandpileEmulator.SeService;

namespace SandpileEmulator
{
    public static class BroadcastingHelper
    {
        public static long SelfChannelId { get; set; }
        public static long SubscribedChannelId { get; set; }
        private static long LastActionId { get; set; }
        private static Timer timer;
        private static ActionModel locker = new ActionModel();
        public static EventHandler<object> ChannelClosed;

        public static void ListenChannel(ChannelModel channel)
        {
            LastActionId = 0;
            SubscribedChannelId = channel.Id;
            timer = new Timer(1000);
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            lock (locker)
            {
                if (SubscribedChannelId > 0)
                {
                    using (var proxy = new SeServiceClient())
                    {
                        var action = proxy.GetNextAction(SubscribedChannelId, LastActionId);
                        if (action != null)
                        {
                            if (action.TypeId == ActionType.ChannelClosed)
                            {
                                if (ChannelClosed != null)
                                {
                                    ChannelClosed(null, null);
                                }
                                DisconnectFromChannel();
                            }
                            else if (action.TypeId == ActionType.LoadGrid)
                            {
                                App.Current.Dispatcher.Invoke(() => GuiHelper.LoadFromText(action.Data));

                            }
                            else if (action.TypeId == ActionType.AddGrain)
                            {
                                App.Current.Dispatcher.Invoke(() =>
                                {
                                    GuiHelper.AddGrain(Convert.ToInt32(action.Data.Split(';')[0]),
                                        Convert.ToInt32(action.Data.Split(';')[1]),
                                        Convert.ToInt32(action.Data.Split(';')[2]));
                                    GuiHelper.DrawSandpileModel();

                                });
                            }
                            BroadcastingHelper.LastActionId = action.Id;
                        }
                        else
                        {
                            //No new action has been done yet
                        }
                    }
                }
            }
        }

        public static void EndBroadcasting()
        {
            if (BroadcastingHelper.SelfChannelId > 0)
            {
                var proxy = new SeServiceClient();
                proxy.EndBroadcastingAsync(BroadcastingHelper.SelfChannelId);
                proxy.Close();
                BroadcastingHelper.SelfChannelId = 0;
            }
        }

        public static void DisconnectFromChannel()
        {
            timer.Stop();
            SubscribedChannelId = 0;            
        }
    }
}
