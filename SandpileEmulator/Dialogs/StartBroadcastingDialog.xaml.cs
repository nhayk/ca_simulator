﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SandpileEmulator.SeService;

namespace SandpileEmulator
{
    /// <summary>
    /// Interaction logic for StartBroadcastingDialog.xaml
    /// </summary>
    public partial class StartBroadcastingDialog : Window
    {
        public EventHandler<string> BroadcastingStarted;

        public StartBroadcastingDialog()
        {
            InitializeComponent();
        }

        private void Start_OnClick(object sender, RoutedEventArgs e)
        {
            if (GuiHelper.Points == null || GuiHelper.Points.Count <= 0)
            {
                MessageBox.Show("Can not start broadcasting with empty model", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (string.IsNullOrEmpty(tbName.Text))
            {
                MessageBox.Show("Name is required", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            using (var proxy = new SeServiceClient())
            {
                var channelId = proxy.StartBroadcasting(tbName.Text);
                BroadcastingHelper.SelfChannelId = channelId;
                string txt = GuiHelper.WriteToText();
                proxy.AddAction(channelId, ActionType.LoadGrid, txt);
                if (BroadcastingStarted != null)
                {
                    BroadcastingStarted(this, tbName.Text);
                }

                using (var proxy2 = new SeServiceClient())
                {
                    var channels = proxy2.GetActiveChannels();
                    var channel = channels.FirstOrDefault(c => c.Id == channelId);
                    if(channel != null)
                    {
                        BroadcastingHelper.ListenChannel(channel);
                    }
                }

            }
           
            this.Close();
        }
    }
}
