﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SandpileEmulator
{
    /// <summary>
    /// Interaction logic for NewModelDialog.xaml
    /// </summary>
    public partial class NewModelDialog : Window
    {
        public NewModelDialog()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            int size = 7;
            if (int.TryParse(tbSize.Text, out size))
            {
                GuiHelper.CreateNewModel(size);
                GuiHelper.DrawSandpileModel();
            }
            else
            {
                MessageBox.Show("Wrong size", "Error", MessageBoxButton.OK, MessageBoxImage.Error);                
            }
            this.Close();
        }
    }
}
