﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using SandpileEmulator.SeService;

namespace SandpileEmulator
{
    /// <summary>
    /// Interaction logic for AddGrainDialog.xaml
    /// </summary>
    public partial class AddGrainDialog : Window
    {
        public AddGrainDialog()
        {
            InitializeComponent();
        }

        private void AddGrain_OnClick(object sender, RoutedEventArgs e)
        {
            int x, y, z, count;
            if (int.TryParse(tbX.Text, out x)
                && int.TryParse(tbY.Text, out y)
                && int.TryParse(tbZ.Text, out z)
                && int.TryParse(tbCount.Text, out count))
            {
                
                if (BroadcastingHelper.SelfChannelId > 0)
                {
                    using (var proxy = new SeServiceClient())
                    {
                        proxy.AddActionAsync(BroadcastingHelper.SelfChannelId, ActionType.AddGrain, string.Format("{0};{1};{2}", x, y, z));
                    }
                }

                else if(BroadcastingHelper.SubscribedChannelId > 0)
                {
                    using (var proxy = new SeServiceClient())
                    {
                        proxy.AddActionAsync(BroadcastingHelper.SubscribedChannelId, ActionType.AddGrain, string.Format("{0};{1};{2}", x, y, z));
                    }
                } 
                else
                {
                    GuiHelper.AddGrain(x, y, z);
                    GuiHelper.DrawSandpileModel();
                }
            }
            else
            {
                MessageBox.Show("Wrong position", "Error", MessageBoxButton.OK, MessageBoxImage.Error);                
            }
        }
    }
}
