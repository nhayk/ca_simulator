﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SandpileEmulator.SeService;

namespace SandpileEmulator
{
    /// <summary>
    /// Interaction logic for ConnectToChannelDialog.xaml
    /// </summary>
    public partial class ConnectToChannelDialog : Window
    {
        public EventHandler<string> ConnectedToChannel;

        public ConnectToChannelDialog()
        {
            InitializeComponent();
            LoadChannels();
        }

        public long ChannelId { get; set; }
        public List<ChannelModel> Channels { get; set; }

        private void LoadChannels()
        {
            using (var proxy = new SeServiceClient())
            {
                Channels = proxy.GetActiveChannels();
                dgChannels.ItemsSource = Channels;
            }
        }

        private void DgChannels_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var model = dgChannels.SelectedItem as ChannelModel;
            if (model != null)
            {
                BroadcastingHelper.ListenChannel(model);
                if (ConnectedToChannel != null)
                {
                    ConnectedToChannel(this, model.Name);
                }
                Close();
            }
        }
    }
}