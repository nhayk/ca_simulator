﻿using System;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using SandpileEmulator.SeService;

namespace SandpileEmulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GuiHelper.Init(mainViewPort);
            GuiHelper.GrainAdded += GuiHelper_GrainAdded;
            sliderMenu.Value = 100;            
        }

        private void GuiHelper_GrainAdded(object sender, EventArgs e)
        {
            CountGridAttributesSolidity();
        }

        #region File Menu

        private void MiNewModel_OnClick(object sender, RoutedEventArgs e)
        {
            if (BroadcastingHelper.SelfChannelId > 0 || BroadcastingHelper.SubscribedChannelId > 0)
            {
                MessageBox.Show("It is not allowed to create or load new model during broadcasting/Connected to Channel", "",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            NewModelDialog dialog = new NewModelDialog();           
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            dialog.ShowDialog();
        }

        private void MiLoadModel_OnClick(object sender, RoutedEventArgs e)
        {
            if (BroadcastingHelper.SelfChannelId > 0 || BroadcastingHelper.SubscribedChannelId > 0)
            {
                MessageBox.Show("It is not allowed to create or load new model during broadcasting/Connected to Channel", "",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (GuiHelper.Points != null && GuiHelper.Points.Count > 0)
            {
                if (MessageBox.Show("You have unfinished work. All data will be lost. Continue?",
                    "", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    return;
                }
            }

            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                GuiHelper.LoadFromFile(dialog.FileName);
            }
        }

        private void MiSaveModel_OnClick(object sender, RoutedEventArgs e)
        {
            if (GuiHelper.Points == null || GuiHelper.Points.Count <= 0)
            {
                MessageBox.Show("Can`t save empty model", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.AddExtension = true;
            dialog.DefaultExt = "xml";
            if (dialog.ShowDialog() == true)
            {
                GuiHelper.WriteToFile(dialog.FileName);
            }

        }

        #endregion

        #region Edit Menu

        private void AddGrain_OnClick(object sender, RoutedEventArgs e)
        {
            /*if (BroadcastingHelper.SubscribedChannelId > 0)
            {
                MessageBox.Show("It is not allowed to add grain during when connected to Channel", "",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }*/
            AddGrainDialog dialog = new AddGrainDialog();           
            dialog.ShowDialog();
        }


        private void ShowLayer_OnClick(object sender, RoutedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Broadcasting Menu

        private void MiStartBroadcasting_OnClick(object sender, RoutedEventArgs e)
        {
            if (BroadcastingHelper.SelfChannelId > 0)
            {                
                BroadcastingHelper.EndBroadcasting();
                tbBroadcastingInfo.Text = "";
                MiStartBroadcasting.Header = "Start Broadcasting";
            }
            else
            {
                if (BroadcastingHelper.SubscribedChannelId > 0)
                {
                    MessageBox.Show("You are connected to channel. Disconnect from channel at first.", "",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                
                var dialog = new StartBroadcastingDialog();
                dialog.BroadcastingStarted += (o, s) =>
                {
                    tbBroadcastingInfo.Text = string.Format("Broadcasting Channel {0}", s);
                    MiStartBroadcasting.Header = "End Broadcasting";
                };
                dialog.ShowDialog();               
            }
        }

        private void MiConnectToChannel_OnClick(object sender, RoutedEventArgs e)
        {
            if (BroadcastingHelper.SubscribedChannelId > 0)
            {
                BroadcastingHelper.DisconnectFromChannel();
                MiConnectToChannel.Header = "Connect to Channel";
                tbBroadcastingInfo.Text = "";
            }
            else
            {
                if (BroadcastingHelper.SelfChannelId > 0)
                {
                    MessageBox.Show("You are broadcasting to channel. End broadcasting at first.", "",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                BroadcastingHelper.ChannelClosed += (o, o1) =>
                {
                    App.Current.Dispatcher.Invoke(() =>
                    {
                        tbBroadcastingInfo.Text = "";
                        MiConnectToChannel.Header = "Connect to Channel";
                    });
                };

                if (GuiHelper.Points != null && GuiHelper.Points.Count > 0)
                {
                    if (MessageBox.Show("You have unfinished work. All data will be lost. Continue?",
                        "", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    {
                        return;
                    }
                }

                ConnectToChannelDialog dialog = new ConnectToChannelDialog();
                dialog.ConnectedToChannel += (o, s) =>
                {
                    tbBroadcastingInfo.Text = string.Format("Connected to Channel {0}", s);
                    MiConnectToChannel.Header = "Disconnect from Channel";
                };
                dialog.ShowDialog();
            }


        }

        #endregion

        protected override void OnClosed(EventArgs e)
        {
            BroadcastingHelper.EndBroadcasting();           
            base.OnClosed(e);
        }
        private void CountGridAttributesSolidity()
        {
            int totalGrains = 0;
            bool isStable = true;
            double avarageSolidity;

            int height_1 = 0;
            int height_2 = 0;
            int height_3 = 0;
            int height_4 = 0;
            int height_5 = 0;
            int height_6 = 0;
            int height_7 = 0;

            for (int i = 0; i < GuiHelper.Points.Count; ++i)
            {
                switch (GuiHelper.Points.ElementAt(i).Weight)
                {
                    case 1:
                        height_1++;
                        totalGrains += 1;
                        break;
                    case 2:
                        height_2++;
                        totalGrains += 2;
                        break;
                    case 3:
                        height_3++;
                        totalGrains += 3;
                        break;
                    case 4:
                        height_4++;
                        totalGrains += 4;
                        break;
                    case 5:
                        height_5++;
                        totalGrains += 5;
                        break;
                    case 6:
                        height_6++;
                        totalGrains += 6;
                        break;
                    case 7:
                        height_7++;
                        totalGrains += 7;
                        isStable = false;
                        break;

                }

            }

            avarageSolidity = totalGrains / GuiHelper.Points.Count;

            avarage_solidity.Text = "Avarage Solidity: " + avarageSolidity;
            is_stable.Text = "Is Stable:" + isStable.ToString();
            nodes_with_height_1.Text = "Nodes with height 1: " + height_1;
            nodes_with_height_2.Text = "Nodes with height 2: " + height_2;
            nodes_with_height_3.Text = "Nodes with height 3: " + height_3;
            nodes_with_height_4.Text = "Nodes with height 4: " + height_4;
            nodes_with_height_5.Text = "Nodes with height 5: " + height_5;
            nodes_with_height_6.Text = "Nodes with height 6: " + height_6;
            nodes_with_height_7.Text = "Nodes with height 7: " + height_7;

        }
    }
}
