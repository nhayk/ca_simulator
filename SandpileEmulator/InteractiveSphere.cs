﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Media3D;
using _3DTools;
using System.Xml.Serialization;

namespace SandpileEmulator
{
    public class InteractiveSphere : InteractiveVisual3D, IXmlSerializable
    {
        public InteractiveSphere()
        {

        }

        public InteractiveSphere(Point3D position)
        {
            _position = position;
        }

        #region "Position"
        private Point3D _position;
        public Point3D Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        #endregion "Position"

        #region "Weight"
        private int _weight;
        public int Weight
        {
            get
            {
                return _weight;
            }
            set
            {
               _weight = value;  
            }
        }
        #endregion "Weight"

        public void Draw()
        {            
            Geometry = Tessellate();
        }

        private MeshGeometry3D Tessellate()
        {
            MeshGeometry3D mesh = new MeshGeometry3D();

            mesh.Positions.Add(new Point3D(0, 0, 0));
            mesh.Positions.Add(new Point3D(1, 0, 0));
            mesh.Positions.Add(new Point3D(0, 1, 0));
            mesh.Positions.Add(new Point3D(1, 1, 0));
            mesh.Positions.Add(new Point3D(0, 0, 1));
            mesh.Positions.Add(new Point3D(1, 0, 1));
            mesh.Positions.Add(new Point3D(0, 1, 1));
            mesh.Positions.Add(new Point3D(1, 1, 1));

            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(4);

            mesh.Freeze();
            return mesh;
        }


        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            string strPosition = reader.GetAttribute("Position");
            string strWeight = reader.GetAttribute("Weight");
            if (!string.IsNullOrEmpty(strPosition))
            {
                Position = new Point3D(Convert.ToDouble(strPosition.Split(';')[0]),
                    Convert.ToDouble(strPosition.Split(';')[1]), Convert.ToDouble(strPosition.Split(';')[2]));
            }
            if (!string.IsNullOrEmpty(strWeight))
            {
                Weight = Convert.ToInt32(strWeight);
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("Position", string.Format("{0};{1};{2}", Position.X, Position.Y, Position.Z));
            writer.WriteAttributeString("Weight", string.Format("{0}", Weight));       
        }
    }
}