﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandpileService
{
    public enum ActionType
    {
        LoadGrid = 1,
        AddGrain = 2,
        ChannelClosed = 3
    }
}