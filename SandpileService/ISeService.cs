﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SandpileService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ISeService
    {
        [OperationContract]
        long StartBroadcasting(string name);

        [OperationContract]
        void EndBroadcasting(long id);

        [OperationContract]
        void AddAction(long channelId, ActionType type, string data);

        [OperationContract]
        ActionModel GetNextAction(long channelId, long lastActionId);

        [OperationContract]
        List<ChannelModel> GetActiveChannels();
    }
}
