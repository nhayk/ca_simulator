﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SandpileService
{
    [DataContract]
    public class ChannelModel
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime StartedOn { get; set; }
        [DataMember]
        public DateTime? EndedOn { get; set; }
    }
}