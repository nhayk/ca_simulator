﻿using System.Runtime.Serialization;

namespace SandpileService
{
    [DataContract]
    public class ActionModel
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ChannelId { get; set; }
        [DataMember]
        public ActionType TypeId { get; set; }
        [DataMember]
        public string Data { get; set; }
    }
}