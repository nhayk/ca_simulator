﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Configuration;

namespace SandpileService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class SeService : ISeService
    {
        public long StartBroadcasting(string name)
        {
            long id = 0;
            using (var dbConnection = new SQLiteConnection(GetDbConnectionString()))
            {
                dbConnection.Open();
                string sql = string.Format(@"INSERT INTO [Channel] (StartedOn, Name) SELECT datetime('now'), '{0}';
                                       SELECT last_insert_rowid();", name);
                using (SQLiteCommand command = new SQLiteCommand(sql, dbConnection))
                {
                    id = (long) command.ExecuteScalar();
                }
            }
            return id;
        }

        public void AddAction(long channelId, ActionType type, string data)
        {
            using (var dbConnection = new SQLiteConnection(GetDbConnectionString()))
            {
                dbConnection.Open();
                string sql = string.Format(@"INSERT INTO [Action] (TypeId, ChannelId, Data) SELECT {0}, {1}, '{2}'"
                    , (int) type, channelId, data);
                using (SQLiteCommand command = new SQLiteCommand(sql, dbConnection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<ChannelModel> GetActiveChannels()
        {
            var channels = new List<ChannelModel>();
            using (var dbConnection = new SQLiteConnection(GetDbConnectionString()))
            {
                dbConnection.Open();
                string sql = @"SELECT * FROM [Channel] ORDER BY StartedOn DESC";
                using (SQLiteCommand cmd = new SQLiteCommand(sql, dbConnection))
                {
                    SQLiteDataReader reader = cmd.ExecuteReader();                    
                    while (reader.Read())
                    {
                        channels.Add(
                            new ChannelModel
                            {
                                Id = Convert.ToInt64(reader["Id"]),
                                Name = Convert.ToString(reader["Name"]),
                                StartedOn = Convert.ToDateTime(reader["StartedOn"]),
                                EndedOn = reader["EndedOn"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["EndedOn"])
                            });
                    }
                    reader.Close();
                }
            }
            return channels;
        }

        public void EndBroadcasting(long id)
        {
            using (var dbConnection = new SQLiteConnection(GetDbConnectionString()))
            {
                dbConnection.Open();
                string sql = string.Format(@"UPDATE [Channel] SET [EndedOn] = datetime('now') WHERE [Id] = {0};", id);
                using (SQLiteCommand command = new SQLiteCommand(sql, dbConnection))
                {

                    command.ExecuteNonQuery();
                }
            }
        }

        public ActionModel GetNextAction(long channelId, long lastActionId)
        {
            using (var dbConnection = new SQLiteConnection(GetDbConnectionString()))
            {
                dbConnection.Open();
                string sql = string.Format(@"SELECT COUNT(*) FROM [Channel] WHERE [Id] = {0} AND [EndedOn] IS NULL",
                    channelId);
                using (SQLiteCommand cmd = new SQLiteCommand(sql, dbConnection))
                {
                    var count = Convert.ToInt32(cmd.ExecuteScalar());
                    if (count > 0)
                    {
                        sql =
                            string.Format(
                                @"SELECT * FROM [Action] WHERE [ChannelId] = {0} AND [Id] > {1} ORDER BY [Id] LIMIT 1 ",
                                channelId, lastActionId);
                        using (var cmd2 = new SQLiteCommand(sql, dbConnection))
                        {
                            using (SQLiteDataReader reader = cmd2.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    return new ActionModel
                                    {
                                        Id = Convert.ToInt64(reader["Id"]),
                                        TypeId = (ActionType) Convert.ToInt64(reader["TypeId"]),
                                        ChannelId = Convert.ToInt64(reader["ChannelId"]),
                                        Data = Convert.ToString(reader["Data"])
                                    };
                                }
                            }
                        }
                    }
                    else
                    {
                        return new ActionModel {TypeId = ActionType.ChannelClosed};
                    }
                }
            }

            return null;
        }

        private string GetDbConnectionString()
        {            
            return ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString;
        }

    }
}
